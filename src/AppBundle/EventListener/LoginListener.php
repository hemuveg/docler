<?php

namespace AppBundle\EventListener;

use AppBundle\Util\CaptchaUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListener
{

  /**
   * @var ContainerInterface
   */
  private $container;

  /**
   * @var TokenStorage
   */
  private $securityContext;

  /**
   *
   * @var CaptchaUtil
   */
  private $captchaUtil;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
    $this->securityContext = $container->get('security.token_storage');
    $this->captchaUtil = new CaptchaUtil($this->container);
  }

  /**
   * onAuthenticationSuccess
   *
   * @param     InteractiveLoginEvent $event
   */
  public function onAuthenticationSuccess(InteractiveLoginEvent $event)
  {
    $request = $event->getRequest();

    if ( $this->captchaUtil->captchaNeeded() && $request->request->get('login') )
    {
      $postdata = $request->request->get('login');
      $session = $request->getSession();
      $captchaData = $session->get('gcb_captcha');
      if ( $captchaData['phrase'] != $postdata['captcha'] )
      {
        $this->securityContext->setToken(null);
        $request->getSession()->invalidate();
        $error = $session->set(Security::AUTHENTICATION_ERROR, new AuthenticationException('Bad code value'));
      }
      else
      {
        $this->captchaUtil->resetFailedLogin($request);
      }
    }
  }

  public function onAuthenticationFailure(AuthenticationFailureEvent $event)
  {
    /* @var $request Request */
    $request = $this->container->get('request_stack')->getCurrentRequest();

    $this->captchaUtil->addFailedLogin($request);
  }

}

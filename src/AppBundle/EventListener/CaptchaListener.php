<?php

namespace AppBundle\EventListener;

use AppBundle\Controller\CaptchaHandlerInterface;
use AppBundle\Util\CaptchaUtil;
use Doctrine\Common\Util\Debug;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class CaptchaListener
{

  /**
   * @var ContainerInterface
   */
  private $container;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }

  public function onKernelController(FilterControllerEvent $event)
  {
    $controller = $event->getController();

    if ( !is_array($controller) )
    {
      return;
    }

    if ( $controller[0] instanceof CaptchaHandlerInterface )
    {
      $captchaUtil = new CaptchaUtil($this->container);
      $captchaUtil->updateSession();
    }
  }

}

<?php

namespace AppBundle\Entity;

/**
 * LoginFailure
 */
class LoginFailure
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var integer
     */
    private $count;

    /**
     * @var \DateTime
     */
    private $last_date;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return LoginFailure
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return LoginFailure
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return LoginFailure
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set lastDate
     *
     * @param \DateTime $lastDate
     *
     * @return LoginFailure
     */
    public function setLastDate($lastDate)
    {
        $this->last_date = $lastDate;

        return $this;
    }

    /**
     * Get lastDate
     *
     * @return \DateTime
     */
    public function getLastDate()
    {
        return $this->last_date;
    }
    /**
     * @var string
     */
    private $log_type;


    /**
     * Set logType
     *
     * @param string $logType
     *
     * @return LoginFailure
     */
    public function setLogType($logType)
    {
        $this->log_type = $logType;

        return $this;
    }

    /**
     * Get logType
     *
     * @return string
     */
    public function getLogType()
    {
        return $this->log_type;
    }
}

<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class LoginFailureRepository extends EntityRepository
{
  const LOG_TYPE_USERNAME = 'username';
  const LOG_TYPE_IP       = 'ipaddress';
  const LOG_TYPE_RANGE24  = 'ip24';
  const LOG_TYPE_RANGE16  = 'ip16';

  const LIMIT_BY_USERNAME = 2;
  const LIMIT_BY_IP       = 2;
  const LIMIT_BY_RANGE24  = 499;
  const LIMIT_BY_RANGE16  = 999;

  /**
   * Gets or creates the LoginFailure entity for the given username
   * @param string $username
   * @return \AppBundle\Entity\LoginFailure
   */
  public function getLastByUsername($username)
  {
    $qb = $this->createQueryBuilder('f');
    $last = $qb
      ->andWhere($qb->expr()->eq('f.username', ':username'))
        ->setParameter('username', $username)
      ->andWhere($qb->expr()->eq('f.log_type', ':type'))
        ->setParameter('type', self::LOG_TYPE_USERNAME)
      ->andWhere($qb->expr()->gte('f.last_date', ':last_hour'))
        ->setParameter('last_hour', date('Y-m-d H:i:s', time() - 3600))
      ->getQuery()
      ->getOneOrNullResult();

    if ( !$last )
    {
      $class = $this->getEntityName();
      $last = new $class;
      $last
        ->setUsername($username)
        ->setLogType(self::LOG_TYPE_USERNAME)
        ->setLastDate(new \DateTime())
      ;
    }

    return $last;
  }

  /**
   * Gets or creates the LoginFailure entity for the given ip
   * @param string $ipaddress
   * @return \AppBundle\Entity\LoginFailure
   */
  public function getLastByIp($ipaddress)
  {
    $qb = $this->createQueryBuilder('f');
    $last = $qb
      ->andWhere($qb->expr()->eq('f.ip', ':ip'))
        ->setParameter('ip', $ipaddress)
      ->andWhere($qb->expr()->eq('f.log_type', ':type'))
        ->setParameter('type', self::LOG_TYPE_IP)
      ->andWhere($qb->expr()->gte('f.last_date', ':last_hour'))
        ->setParameter('last_hour', date('Y-m-d H:i:s', time() - 3600))
      ->getQuery()
      ->getOneOrNullResult();

    if ( !$last )
    {
      $class = $this->getEntityName();
      $last = new $class;
      $last
        ->setIp($ipaddress)
        ->setLogType(self::LOG_TYPE_IP)
        ->setLastDate(new \DateTime())
      ;
    }

    return $last;
  }

  /**
   * Gets or creates the LoginFailure entity for the given ip/24
   * @param string $ipaddress
   * @return \AppBundle\Entity\LoginFailure
   */
  public function getLastByRange24($ipaddress)
  {
    $ipaddress = $this->getRange24($ipaddress);
    $qb = $this->createQueryBuilder('f');
    $last = $qb
      ->andWhere($qb->expr()->eq('f.ip', ':ip'))
        ->setParameter('ip', $ipaddress)
      ->andWhere($qb->expr()->eq('f.log_type', ':type'))
        ->setParameter('type', self::LOG_TYPE_RANGE24)
      ->andWhere($qb->expr()->gte('f.last_date', ':last_hour'))
        ->setParameter('last_hour', date('Y-m-d H:i:s', time() - 3600))
      ->getQuery()
      ->getOneOrNullResult();

    if ( !$last )
    {
      $class = $this->getEntityName();
      $last = new $class;
      $last
        ->setIp($ipaddress)
        ->setLogType(self::LOG_TYPE_RANGE24)
        ->setLastDate(new \DateTime())
      ;
    }

    return $last;
  }

  /**
   * Gets or creates the LoginFailure entity for the given ip/16
   * @param string $ipaddress
   * @return \AppBundle\Entity\LoginFailure
   */
  public function getLastByRange16($ipaddress)
  {
    $ipaddress = $this->getRange16($ipaddress);
    $qb = $this->createQueryBuilder('f');
    $last = $qb
      ->andWhere($qb->expr()->eq('f.ip', ':ip'))
        ->setParameter('ip', $ipaddress)
      ->andWhere($qb->expr()->eq('f.log_type', ':type'))
        ->setParameter('type', self::LOG_TYPE_RANGE16)
      ->andWhere($qb->expr()->gte('f.last_date', ':last_hour'))
        ->setParameter('last_hour', date('Y-m-d H:i:s', time() - 3600))
      ->getQuery()
      ->getOneOrNullResult();

    if ( !$last )
    {
      $class = $this->getEntityName();
      $last = new $class;
      $last
        ->setIp($ipaddress)
        ->setLogType(self::LOG_TYPE_RANGE16)
        ->setLastDate(new \DateTime())
      ;
    }

    return $last;
  }

  /**
   * Happens when successfull login 
   * Removes failed login entries for this username
   * @param string $username
   */
  public function removeAllByUsername($username)
  {
    $qb = $this->createQueryBuilder('f');
    $qb
      ->delete()
      ->andWhere($qb->expr()->eq('f.log_type', ':type'))
        ->setParameter('type', self::LOG_TYPE_USERNAME)
      ->andWhere($qb->expr()->eq('f.username', ':username'))
        ->setParameter('username', $username)
      ->getQuery()->execute();
  }

  /**
   * Happens when successfull login 
   * Removes failed login entries for this ip
   * @param string $ipaddress
   */
  public function removeAllByIp($ipaddress)
  {
    $qb = $this->createQueryBuilder('f');
    $qb
      ->delete()
      ->andWhere($qb->expr()->eq('f.log_type', ':type'))
        ->setParameter('type', self::LOG_TYPE_IP)
      ->andWhere($qb->expr()->eq('f.ip', ':ip'))
        ->setParameter('ip', $ipaddress)
      ->getQuery()->execute();
  }

  /**
   * Removes all entries older than 1 hour when successfull login
   */
  public function removeAllOld()
  {
    $qb = $this->createQueryBuilder('f');
    $qb
      ->delete()
      ->andWhere($qb->expr()->lt('f.last_date', ':last_date'))
        ->setParameter('last_date', date('Y-m-d H:i:s', time() - 3600))
      ->getQuery()->execute();
  }

  /**
   * Checks if current user/ip has enough failed logins for captcha
   * @param string $username
   * @param string $ipaddress
   * @return boolean
   */
  public function hasFailedLogins($username = null, $ipaddress)
  {
    // by username
    if ( $username )
    {
      $qb = $this->createQueryBuilder('f');
      $res = $qb
        ->select('SUM(f.count)')
        ->andWhere($qb->expr()->eq('f.username', ':username'))
          ->setParameter('username', $username)
        ->andWhere($qb->expr()->gt('f.last_date', ':last_date'))
          ->setParameter('last_date', date('Y-m-d H:i:s', time() - 3600))
        ->andWhere($qb->expr()->eq('f.log_type', ':type'))
          ->setParameter('type', self::LOG_TYPE_USERNAME)
        ->getQuery()->execute(null, Query::HYDRATE_SINGLE_SCALAR);
      if ( $res > self::LIMIT_BY_USERNAME )
      {
        return true;
      }
    }

    // by ip
    $qb = $this->createQueryBuilder('f');
    $res = $qb
      ->select('SUM(f.count)')
      ->andWhere($qb->expr()->eq('f.ip', ':ip'))
        ->setParameter('ip', $ipaddress)
      ->andWhere($qb->expr()->gt('f.last_date', ':last_date'))
        ->setParameter('last_date', date('Y-m-d H:i:s', time() - 3600))
        ->andWhere($qb->expr()->eq('f.log_type', ':type'))
          ->setParameter('type', self::LOG_TYPE_IP)
      ->getQuery()->execute(null, Query::HYDRATE_SINGLE_SCALAR);
    if ( $res > self::LIMIT_BY_IP )
    {
      return true;
    }

    // by range24
    $ipaddress = $this->getRange24($ipaddress);
    $qb = $this->createQueryBuilder('f');
    $res = $qb
      ->select('SUM(f.count)')
      ->andWhere($qb->expr()->eq('f.ip', ':ip'))
        ->setParameter('ip', $ipaddress)
      ->andWhere($qb->expr()->gt('f.last_date', ':last_date'))
        ->setParameter('last_date', date('Y-m-d H:i:s', time() - 3600))
        ->andWhere($qb->expr()->eq('f.log_type', ':type'))
          ->setParameter('type', self::LOG_TYPE_RANGE24)
      ->getQuery()->execute(null, Query::HYDRATE_SINGLE_SCALAR);
    if ( $res > self::LIMIT_BY_RANGE24 )
    {
      return true;
    }

    // by range16
    $ipaddress = $this->getRange16($ipaddress);
    $qb = $this->createQueryBuilder('f');
    $res = $qb
      ->select('SUM(f.count)')
      ->andWhere($qb->expr()->eq('f.ip', ':ip'))
        ->setParameter('ip', $ipaddress)
      ->andWhere($qb->expr()->gt('f.last_date', ':last_date'))
        ->setParameter('last_date', date('Y-m-d H:i:s', time() - 3600))
        ->andWhere($qb->expr()->eq('f.log_type', ':type'))
          ->setParameter('type', self::LOG_TYPE_RANGE16)
      ->getQuery()->execute(null, Query::HYDRATE_SINGLE_SCALAR);

    if ( $res > self::LIMIT_BY_RANGE16 )
    {
      return true;
    }

    return false;
  }

  /**
   * Returns the modified ip address for ip/24
   * @param string $ipaddress
   * @return string
   */
  private function getRange24($ipaddress)
  {
    $ipaddressarr = explode('.', $ipaddress);

    return $ipaddressarr[0] . '.' . $ipaddressarr[1] . '.' . $ipaddressarr[2] . '.0';
  }

  /**
   * Returns the modified ip address for ip/16
   * @param string $ipaddress
   * @return string
   */
  private function getRange16($ipaddress)
  {
    $ipaddressarr = explode('.', $ipaddress);

    return $ipaddressarr[0] . '.' . $ipaddressarr[1] . '.0.0';
  }
}

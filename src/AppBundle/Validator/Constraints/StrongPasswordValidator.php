<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class StrongPasswordValidator extends ConstraintValidator
{
  /**
   * Minimum password length
   * @var integer
   */
  private $passwordLength;

  /**
   * Min. numeric character count in password
   * @var integer
   */
  private $numberCount;

  /**
   * Min. uppercase character count in password
   * @var integer
   */
  private $uppercaseCount;

  /**
   * Min. lowercase character count in password
   * @var integer 
   */
  private $lowercaseCount;

  const PATTERN_LOWER  = '/^(?=.*[A-Z].*[A-Z])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/';

  public function validate($value, Constraint $constraint)
  {
    $pattern = $this->getPatternConfigured();
    if ( !preg_match($pattern, $value, $matches) )
    {
      $this->context->buildViolation($constraint->message)
        ->setParameter('%password_length%', $this->getPasswordLength())
        ->setParameter('%number_count%', $this->getNumberCount())
        ->setParameter('%uppercase_count%', $this->getUppercaseCount())
        ->setParameter('%lowercase_count%', $this->getLowercaseCount())
        ->addViolation();
    }
  }

  public function setParams($config)
  {
    $this
      ->setPasswordLength($config['password_length'])
      ->setNumberCount($config['number_count'])
      ->setUppercaseCount($config['uppercase_count'])
      ->setLowercaseCount($config['lowercase_count'])
    ;
  }

  private function getPatternConfigured()
  {
    // /^(?=.*[A-Z].*[A-Z])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$/
    $pattern = '/^';
    
    // number count
    $pattern .= '(?=';
    for ( $i = 0;$i < $this->getNumberCount(); $i++ )
    {
      $pattern .= '.*[0-9]';
    }
    $pattern .= ')';

    // upper count
    $pattern .= '(?=';
    for ( $i = 0;$i < $this->getUppercaseCount(); $i++ )
    {
      $pattern .= '.*[A-Z]';
    }
    $pattern .= ')';

    // lower count
    $pattern .= '(?=';
    for ( $i = 0;$i < $this->getUppercaseCount(); $i++ )
    {
      $pattern .= '.*[a-z]';
    }
    $pattern .= ')';

    // password length
    $pattern .= '.{' . $this->getPasswordLength() . '}$/';

    return $pattern;
  }

  public function getPasswordLength()
  {
    return $this->passwordLength;
  }

  public function getNumberCount()
  {
    return $this->numberCount;
  }

  public function getUppercaseCount()
  {
    return $this->uppercaseCount;
  }

  public function getLowercaseCount()
  {
    return $this->lowercaseCount;
  }

  public function setPasswordLength($passwordLength)
  {
    $this->passwordLength = $passwordLength;
    return $this;
  }

  public function setNumberCount($numberCount)
  {
    $this->numberCount = $numberCount;
    return $this;
  }

  public function setUppercaseCount($uppercaseCount)
  {
    $this->uppercaseCount = $uppercaseCount;
    return $this;
  }

  public function setLowercaseCount($lowercaseCount)
  {
    $this->lowercaseCount = $lowercaseCount;
    return $this;
  }
}

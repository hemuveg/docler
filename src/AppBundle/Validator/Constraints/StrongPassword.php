<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class StrongPassword extends Constraint
{
  public $message = 'Password must contain min. %uppercase_count% uppercase letters, min. %number_count% numbers, min. %lowercase_count% lowercase letters and must be min. %password_length% characters';

  public function validatedBy()
  {
    return 'app.validator.strongpassword';
  }
}

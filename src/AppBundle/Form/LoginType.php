<?php

namespace AppBundle\Form;

use AppBundle\Util\CaptchaUtil;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginType extends AbstractType
{

  /**
   * @var ContainerInterface
   */
  private $container;

  public function __construct(ContainerInterface $container)
  {
    $this->container = $container;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('username', TextType::class, array(
        'label' => 'security.login.username',
        'translation_domain' => 'FOSUserBundle',
        'attr'  => array(
          'id' => '_username',
          'required' => true,
        ),
      ))
      ->add('password', PasswordType::class, array(
        'label' => 'security.login.password',
        'translation_domain' => 'FOSUserBundle',
        'attr'  => array(
          'id' => '_password',
          'required' => true,
        ),
      ))
    ;
    if ( $this->needsCaptcha() )
    {
      $builder
        ->add('captcha', CaptchaType::class, array(
          'label'  => false,
          'reload' => true,
          'as_url' => true,
          'width'  => 200,
          'height' => 80,
        ))
      ;
    }
    $builder
      ->add('submit', SubmitType::class, array(
        'label' => 'security.login.submit',
        'translation_domain' => 'FOSUserBundle',
      ))
    ;
  }

  private function needsCaptcha()
  {
    $captchaUtil = new CaptchaUtil($this->container);

    return $captchaUtil->captchaNeeded();
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'csrf_protection' => false,
    ));
  }

  public function setDefaultOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'intention' => 'authenticate',
      'cascade_validation' => true
    ));
  }
}

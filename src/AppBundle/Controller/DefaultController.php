<?php

namespace AppBundle\Controller;

use AppBundle\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class DefaultController extends Controller implements \AppBundle\Controller\CaptchaHandlerInterface
{

  public function indexAction(Request $request)
  {
    return $this->render('AppBundle:Default:index.html.twig');
  }

  public function loginAction(Request $request)
  {
    $form = $this->createForm(LoginType::class);

    $error = null;
    if ( $request->getSession()->has(Security::AUTHENTICATION_ERROR) )
    {
      $error = $request->getSession()->get(Security::AUTHENTICATION_ERROR)->getMessage();
      $request->getSession()->remove(Security::AUTHENTICATION_ERROR);
    }

    $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();

    return $this->render('AppBundle:Default:login.html.twig', array(
      'form'       => $form->createView(),
      'csrf_token' => $csrfToken,
      'error'      => $error,
    ));
  }

}

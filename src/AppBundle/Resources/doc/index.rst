Reamde
======

Requirements
------------
* PHP 5.5+
* Symfony 3+
* Mysql 5+
* Internet connection on client computer as 3rd party libraries loaded from CDN

Install
-------
* Install a Symfony Standard Edition (http://symfony.com/doc/current/book/installation.html)

* Copy composer.json into Symfony's root folder

* Update dependencies with composer (https://getcomposer.org/doc/01-basic-usage.md#installing-dependencies)

* Append the following to the end of app/config/config.yml
  fos_user:
    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
    firewall_name: main
    user_class: AppBundle\Entity\User
    registration:
      confirmation:
        enabled: true

  gregwar_captcha: ~

  app:
    password_length: 8
    uppercase_count: 2
    lowercase_count: 3
    number_count   : 2

* Replace app/config/security.yml with the following content:
  security:
    encoders:
      FOS\UserBundle\Model\UserInterface: bcrypt

    role_hierarchy:
      ROLE_ADMIN:       ROLE_USER
      ROLE_SUPER_ADMIN: ROLE_ADMIN

    providers:
      fos_userbundle:
        id: fos_user.user_provider.username

    firewalls:
      dev:
        pattern:  ^/(_(profiler|wdt)|css|images|js)/
        security: false

      main:
        pattern: ^/
        form_login:
          provider: fos_userbundle
          csrf_token_generator: security.csrf.token_manager
          username_parameter: "login[username]"
          password_parameter: "login[password]"
        logout:       true
        anonymous:    true

    access_control:
      - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
      - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
      - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
      - { path: ^/generate-captcha, role: IS_AUTHENTICATED_ANONYMOUSLY }
      - { path: ^/, role: ROLE_USER }

* Create app/config/parameters.yml from parameters.yml.dist and set database variables accordingly
* Create database:
  - php bin/console doctrine:database:create
  - php bin/console doctrine:schema:update --force
  OR
  use the following queries to create tables:
    CREATE TABLE `fos_user` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `enabled` tinyint(1) NOT NULL,
      `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `last_login` datetime DEFAULT NULL,
      `locked` tinyint(1) NOT NULL,
      `expired` tinyint(1) NOT NULL,
      `expires_at` datetime DEFAULT NULL,
      `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `password_requested_at` datetime DEFAULT NULL,
      `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
      `credentials_expired` tinyint(1) NOT NULL,
      `credentials_expire_at` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
      UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    CREATE TABLE `app_login_failure` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
      `count` int(11) NOT NULL,
      `last_date` datetime NOT NULL,
      `log_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      PRIMARY KEY (`id`),
      KEY `username_index` (`username`),
      KEY `ip_index` (`ip`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
  * Update app/config/routing.yml:
    gregwar_captcha_routing:
        resource: "@GregwarCaptchaBundle/Resources/config/routing/routing.yml"
    app:
      resource: "@AppBundle/Resources/config/routing.yml"
    fos_user:
      resource: "@FOSUserBundle/Resources/config/routing/all.xml"

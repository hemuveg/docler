<?php

namespace AppBundle\DependencyInjection\Configuration;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
  public function getConfigTreeBuilder()
  {
    $treebuilder = new TreeBuilder();
    $rootnode = $treebuilder->root('app');

    $rootnode
      ->children()
        ->integerNode('password_length')
          ->min(8)
          ->max(30)
          ->isRequired(true)
          ->defaultValue(8)
        ->end()
        ->integerNode('uppercase_count')
          ->min(1)
          ->isRequired(true)
          ->defaultValue(2)
        ->end()
        ->integerNode('lowercase_count')
          ->min(1)
          ->isRequired(true)
          ->defaultValue(3)
        ->end()
        ->integerNode('number_count')
          ->min(1)
          ->isRequired(true)
          ->defaultValue(2)
        ->end()
      ->end();

    return $treebuilder;
  }
}

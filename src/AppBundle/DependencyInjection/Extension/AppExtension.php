<?php

namespace AppBundle\DependencyInjection\Extension;

use AppBundle\DependencyInjection\Configuration\Configuration;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class AppExtension extends Extension
{

  /**
   * {@inheritdoc}
   */
  public function load(array $configs, ContainerBuilder $container)
  {
    $processor = new Processor();
    $configuration = new Configuration();
    $config = $processor->processConfiguration($configuration, $configs);

    $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../Resources/config'));
    $loader->load('services.yml');

    // provide parameters from config to validator
    $validatorconstraintService = $container->getDefinition('app.user.strongpassword_constraint');
    $validatorconstraintService->addMethodCall('setParams', array($config));
  }

  public function getAlias()
  {
    return 'app';
  }
}

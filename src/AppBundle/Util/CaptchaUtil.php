<?php

namespace AppBundle\Util;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class CaptchaUtil
{

  const SESSION_KEY = 'app_needs_captcha';

  /**
   * @var ContainerInterface
   */
  private $container;

  /**
   * @var EntityManager
   */
  private $em;

  /**
   *
   * @var EntityRepository
   */
  private $repo;

  public function __construct(ContainerInterface $container = null)
  {
    if ( $container )
    {
      $this->setContainer($container);
    }
  }

  public function setContainer(ContainerInterface $container)
  {
    $this->container = $container;
    $this->em = $container->get('doctrine')->getManager();
    $this->repo = $container->get('doctrine')->getRepository('AppBundle\Entity\LoginFailure');
  }

  public function captchaNeeded()
  {
    return $this->container->get('request_stack')->getCurrentRequest()->getSession()->get(self::SESSION_KEY);
  }

  public function addFailedLogin(Request $request)
  {
    $session = $request->getSession();
    $logindata = $request->request->get('login');
    $username = $logindata['username'];
    $ipaddress = $request->server->get('REMOTE_ADDR');

    // login failure by username 
    $log = $this->repo->getLastByUsername($username);
    $log->setCount($log->getCount() + 1);
    $this->em->persist($log);

    // login failure by ip
    $log2 = $this->repo->getLastByIp($ipaddress);
    $log2->setCount($log2->getCount() + 1);
    $this->em->persist($log2);

    // login failure by ranges
    if ( $session->get(self::SESSION_KEY, false) === false )
    {
      $log3 = $this->repo->getLastByRange24($ipaddress);
      $log3->setCount($log3->getCount() + 1);
      $this->em->persist($log3);

      $log4 = $this->repo->getLastByRange16($ipaddress);
      $log4->setCount($log4->getCount() + 1);
      $this->em->persist($log4);
    }
    $this->em->flush();
  }

  public function resetFailedLogin(Request $request)
  {
    $logindata = $request->request->get('login');
    $username = $logindata['username'];
    $ipaddress = $request->server->get('REMOTE_ADDR');

    $this->repo->removeAllByUsername($username);
    $this->repo->removeAllByIp($ipaddress);
    $this->repo->removeAllOld();
  }

  public function updateSession()
  {
    /* @var $request Request */
    $request = $this->container->get('request_stack')->getCurrentRequest();
    /* @var $session Session */
    $session = $request->getSession();
    $username = $session->get('_security.last_username');
    $ipaddress = $request->server->get('REMOTE_ADDR');

    $result = $this->repo->hasFailedLogins($username, $ipaddress);
    $session->set(self::SESSION_KEY, $result);
    $session->save();
  }
}
